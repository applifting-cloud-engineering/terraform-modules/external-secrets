data "google_client_config" "current" {}
data "google_project" "current" {}

locals {
  secret_name     = "${var.name}-${random_id.suffix.hex}"
  k8s_secret_name = var.k8s_secret_name == "" ? var.name : var.k8s_secret_name

  secret_value = [for k, v in var.k8s_secret_keys : {
    "remoteRef" = {
      "key"     = "${var.name}-${replace(k, ".", "-")}-${random_id.suffix.hex}"
      "version" = "latest"
    }
    "secretKey" = k
  }]

}

# Key names cannot be re-used, so this will prevent usage of the same names.
resource "random_id" "suffix" {
  byte_length = 2
}

module "gcp_secret" {
  for_each = var.k8s_secret_keys

  source = "./create-empty-secret-secret-manager"

  project_id = data.google_client_config.current.project
  id         = "${var.name}-${replace(each.key, ".", "-")}-${random_id.suffix.hex}"
  secret     = each.value

  replication_locations = [data.google_client_config.current.region]
}


resource "kubernetes_manifest" "external_secrets_operator" {
  count = var.create_k8s_secret ? 1 : 0

  manifest = {

    "apiVersion" = "external-secrets.io/v1beta1"
    "kind"       = "ExternalSecret"
    "metadata" = {
      "name"        = local.k8s_secret_name
      "namespace"   = var.k8s_secret_namespace
      "annotations" = var.k8s_secret_annotations
      "labels"      = var.k8s_secret_labels
    },
    "spec" = {
      "data"            = local.secret_value
      "refreshInterval" = "60s"
      "secretStoreRef" = {
        "kind" = "ClusterSecretStore",
        "name" = var.secretStore
      }
    }
  }
}
