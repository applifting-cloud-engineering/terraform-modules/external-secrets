output "k8s_secret_name" {
  value       = local.k8s_secret_name
  description = "Name of the Secret created in Kubernetes cluster/namespace."
}