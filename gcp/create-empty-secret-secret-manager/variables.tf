variable "project_id" {
  description = "The GCP project identifier where the secret will be created."
  type        = string
  validation {
    condition     = can(regex("^[a-z][a-z0-9-]{4,28}[a-z0-9]$", var.project_id))
    error_message = "The project_id must be a string of alphanumeric or hyphens, between 6 and 3o characters in length."
  }
}

variable "id" {
  description = "The secret identifier to create; this value must be unique within the project."
  type        = string
  validation {
    condition     = can(regex("^[a-zA-Z0-9_-]{1,255}$", var.id))
    error_message = "The id must be a string of alphanumeric, hyphen, and underscore characters, and upto 255 characters in length."
  }
}

variable "replication_locations" {
  description = " An optional list of replication locations for the secret. If the value is an empty list (default) then an automatic replication policy will be applied. Use this if you must have replication constrained to specific locations."
  type        = list(string)
  default     = []
}

variable "replication_keys" {
  description = "An optional map of customer managed keys per location. This needs to match the locations specified in `replication_locations`."
  type        = map(string)
  default     = {}
}

variable "secret" {
  description = "The secret payload to store in Secret Manager. Binary values should be base64 encoded before use."
  type        = string
  default     = ""
}

variable "accessors" {
  description = "An optional list of IAM account identifiers that will be granted accessor (read-only) permission to the secret."
  type        = list(string)
  default     = []
  validation {
    condition     = length(join("", [for acct in var.accessors : can(regex("^(?:group|serviceAccount|user):[^@]+@[^@]*$", acct)) ? "x" : ""])) == length(var.accessors)
    error_message = "Each accessors value must be a valid IAM account identifier; e.g. user:jdoe@company.com, group:admins@company.com, serviceAccount:service@project.iam.gserviceaccount.com."
  }
}

variable "labels" {
  description = "An optional map of label key:value pairs to assign to the secret resources. Default is an empty map."
  type        = map(string)
  default     = {}
}