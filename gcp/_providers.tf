terraform {
  required_version = "~> 1.0"

  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.7"
    }
    google = {
      source  = "hashicorp/google"
      version = ">= 3.44, < 5"
    }
  }
}