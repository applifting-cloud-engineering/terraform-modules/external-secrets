variable "name" {
  description = "Name of the secret. It is also used as a prefix for naming resources."
  type        = string
}

variable "create_k8s_secret" {
  description = "Indicates whether to create Kubernetes manifest for the External Secret operator. If 'false' only GCP Secret is created."
  type        = bool
  default     = true
}

variable "k8s_secret_name" {
  description = "Name of the Secret created in Kubernetes cluster/namespace. If not specified 'var.name' is used."
  type        = string
  default     = ""
}

variable "k8s_secret_annotations" {
  description = "Annotations for Kubernetes secret"
  type        = map(string)
  default     = {}
}

variable "k8s_secret_labels" {
  description = "Custom labels & selector for Kubernetes secret"
  type        = map(string)
  default     = {}
}

variable "k8s_secret_namespace" {
  description = "Kubernetes namespace where to create the secret."
  type        = string
  default     = "default"
}

variable "k8s_secret_keys" {
  description = "Maps of keys in the secret."
  type        = map(any)
  default     = {}
}

variable "secretStore" {
  description = "Name of the External Secrets Operator SecretStore."
  type        = string
  default     = "gcpsecretsmanager-secretstore"
}