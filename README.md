# External Secrets manifest creator with GCP integration

This module defines and deploys resources in the Google Cloud Platform (GCP) and Kubernetes. A GCP Secret Manager secret is created first and then synced via Kubernetes [ExternalSecret](https://external-secrets.io/v0.7.0/) object in the Kubernetes cluster.

## Description

The code first retrieves information about the current GCP project and configuration using the `google_client_config` and `google_project` data sources.

Next, the code defines a number of local variables that are used later in the code. These variables include `secret_name`, `k8s_secret_name`, `values`, and `new_value_1`.

The code then uses the random_id resource to generate a random hexadecimal string, which is used to create unique names for the secrets and secret keys.

The `module "gcp_secret"` block iterates over the `k8s_secret_keys` variable and creates a secret in GCP Secret Manager for each key. The `id`, `secret`, and `replication_locations` arguments are set using the local variables and data sources defined earlier.

Finally, the `kubernetes_manifest` resource creates an ExternalSecret object in the Kubernetes cluster. The `count` argument is used to conditionally create the resource based on the value of the `create_k8s_secret` variable. The `manifest` argument specifies the properties of the ExternalSecret object, including its name, namespace, data, and reference to the secret store in GCP Secret Manager.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 3.44, < 5 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.7 |
|  [external-secrets](https://charts.external-secrets.io) | ~> 0.5.9 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_k8s_secret"></a> [create\_k8s\_secret](#input\_create\_k8s\_secret) | Indicates whether to create Kubernetes manifest for the External Secret operator. If 'false' only GCP Secret is created. | `bool` | `true` | no |
| <a name="input_k8s_secret_annotations"></a> [k8s\_secret\_annotations](#input\_k8s\_secret\_annotations) | Annotations for Kubernetes secret | `map(string)` | `{}` | no |
| <a name="input_k8s_secret_keys"></a> [k8s\_secret\_keys](#input\_k8s\_secret\_keys) | Maps of keys in the secret. | `map(any)` | `{}` | no |
| <a name="input_k8s_secret_labels"></a> [k8s\_secret\_labels](#input\_k8s\_secret\_labels) | Custom labels & selector for Kubernetes secret | `map(string)` | `{}` | no |
| <a name="input_k8s_secret_name"></a> [k8s\_secret\_name](#input\_k8s\_secret\_name) | Name of the Secret created in Kubernetes cluster/namespace. If not specified 'var.name' is used. | `string` | `""` | no |
| <a name="input_k8s_secret_namespace"></a> [k8s\_secret\_namespace](#input\_k8s\_secret\_namespace) | Kubernetes namespace where to create the secret. | `string` | `"default"` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the secret. It is also used as a prefix for naming resources. | `string` | n/a | yes |
| <a name="input_secretStore"></a> [secretStore](#input\_secretStore) | Name of the External Secrets Operator SecretStore. | `string` | `"gcpsecretsmanager-secretstore"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_k8s_secret_name"></a> [k8s\_secret\_name](#output\_k8s\_secret\_name) | Name of the Secret created in Kubernetes cluster/namespace. |

## Usage
```
module "external_secrets" {
  source = "gitlab.com/applifting-cloud-engineering/external-secrets/gcp"
  version = "2.1.1"

  name                 = "secret-name"
  k8s_secret_keys      = {
    key1="value1",
    key2="value2"
  }
}

```

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 3.44, < 5 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.7 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_gcp_secret"></a> [gcp\_secret](#module\_gcp\_secret) | ./create-empty-secret-secret-manager | n/a |

## Resources

| Name | Type |
|------|------|
| [kubernetes_manifest.external_secrets_operator](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [random_id.suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |
| [google_client_config.current](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/client_config) | data source |
| [google_project.current](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/project) | data source |

